import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-circleprogress',
  templateUrl: './circleprogress.component.html',
  styleUrls: ['./circleprogress.component.scss']
})
export class CircleprogressComponent implements OnInit {

  color = 'warn';
  mode = 'determinate';
  value = '60';
  percent = '70';

  constructor() { }

  ngOnInit() {
  }

}