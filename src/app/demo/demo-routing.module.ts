import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ButtonsComponent } from './buttons/buttons.component';
import { FleboxComponent } from './flebox/flebox.component';
import { CircleprogressComponent } from './circleprogress/circleprogress.component';


const routes: Routes = [
  { path: 'circleprogress', component: CircleprogressComponent },
  { path: 'buttons', component: ButtonsComponent },
  { path: 'flexbox', component: FleboxComponent },
  { path: '**', redirectTo:'circleprogress' }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DemoRoutingModule { }