import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MaterialModule } from '../shared/material.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule } from '@angular/forms';

import { DemoRoutingModule } from './demo-routing.module';
import { ButtonsComponent } from './buttons/buttons.component';
import { FleboxComponent } from './flebox/flebox.component';
import { CircleprogressComponent } from './circleprogress/circleprogress.component';

import { NgCircleProgressModule } from 'ng-circle-progress';

@NgModule({
  declarations: [ButtonsComponent, FleboxComponent, CircleprogressComponent],
  imports: [
    CommonModule,
    MaterialModule,
    FlexLayoutModule,
    FormsModule,
    DemoRoutingModule,
    NgCircleProgressModule.forRoot()
  ]
})
export class DemoModule { }
