import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { MaterialModule } from '../shared/material.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule } from '@angular/forms';

import { ControleprojetosAppComponent } from './controleprojetos-app.component';
import { ToolbarComponent } from './components/toolbar/toolbar.component';
import { CardsComponent } from './components/cards/cards.component';
import { Cadastro } from './components/cadastro/cadastro.component';

const routes: Routes = [
  { path: 'cadastro', component: Cadastro },
  { path: 'controle', component: ControleprojetosAppComponent },
  { path: '**', redirectTo: 'controle' }
];

@NgModule({
  declarations: [ControleprojetosAppComponent, ToolbarComponent, CardsComponent, Cadastro],
  imports: [
    CommonModule,
    MaterialModule,
    FlexLayoutModule,
    FormsModule,
    RouterModule.forChild(routes)
  ]
})
export class ControleprojetosModule { }
