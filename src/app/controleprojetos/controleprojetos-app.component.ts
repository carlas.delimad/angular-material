import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-controleprojetos-app',
  template: `
    <app-cards></app-cards>
  `,
  styles: []
})
export class ControleprojetosAppComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
